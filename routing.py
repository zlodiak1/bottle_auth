from bottle import template, static_file, route, request, redirect
from classes.validator import Validator
from classes.db_connector import DB_connector
from classes.sql_requester import SqlRequester
from classes.hasher import Hasher
from classes.current_user import CurrentUser
import credentials

import datetime

 
validator = Validator()
hasher = Hasher()
current_user = CurrentUser()


def login_required(func):
    def decor_func():
        if current_user.get_current_user() is None:
            redirect('/login')
        return func()
    return decor_func

 
@route('/')
@route('/info')
@login_required
def info():
    return template('templates/info')


@route('/welcome')
@login_required
def welcome():
    return template('templates/welcome')

 
@route('/registration', method=['GET', 'POST'])
def registration():
    form = {
        'email': '',
        'username': ''   
    }

    if request.method == 'POST':
        form = {
            'email': request.forms.get('email').strip(),
            'username': request.forms.get('username').strip(),
            'password1': request.forms.get('password1'),
            'password2': request.forms.get('password2')
        }

        errors_form = validator.check_registration(form)

        if not len(errors_form):
            add_new_user(form)
            user = get_user_by_email(form['email'])
            current_user.set_current_user(user)
            redirect('/welcome')
        else:
            return template('templates/registration', errors_form=errors_form, form=form)
    else:
        return template('templates/registration', form=form)


@route('/login', method=['GET', 'POST'])
def login():
    form = {
        'email': '' 
    }

    if request.method == 'POST':
        form = {
            'email': request.forms.get('email').strip(),
            'password': request.forms.get('password')
        }

        errors_form = validator.check_login(form)

        if not len(errors_form):
            user = get_user_by_email(form['email'])
            if hasher.compare(user[0][3], form['password']):
                current_user.set_current_user(user)
                redirect('/welcome')
            else:
               return template('templates/login', errors_form=['Неверный пароль'], form=form) 
        else:
            return template('templates/login', errors_form=errors_form, form=form)
    else:
        return template('templates/login', form=form)


@route('/static/<filename>', name='static')
def server_static(filename):
    return static_file(filename, root='static')


def add_new_user(form):
    user = {
        'email': form['email'],
        'username': form['username'],
        'password_hash': hasher.get_hash(form['password1']),
        'create_date': datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    }
    
    db_connection = DB_connector(credentials)
    with db_connection as db_connection:
        db_cursor = db_connection.cursor()
        sql_requester = SqlRequester(db_cursor)
        sql_requester.add_user(user)    


def get_user_by_email(email):
    db_connection = DB_connector(credentials)
    with db_connection as db_connection:
        db_cursor = db_connection.cursor()
        sql_requester = SqlRequester(db_cursor)
        user = sql_requester.get_user_by_email(email)
        return user


