import hashlib


class Hasher():
    def get_hash(self, password):
        h = hashlib.sha1(password.encode('utf-8'))
        return h.hexdigest()

    def compare(self, hash, password):
        password_hash = self.get_hash(password)
        return password_hash == hash