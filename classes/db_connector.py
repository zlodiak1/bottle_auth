import psycopg2

class DB_connector:
    def __init__(self, credentials):
        self.credentials = credentials
    
    def __enter__(self):
        self.connection = psycopg2.connect(
            dbname=self.credentials.dbname, 
            user=self.credentials.user, 
            password=self.credentials.password, 
            host=self.credentials.host
        )
        self.connection.autocommit = True
        return self.connection    
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        if hasattr(self, 'connection'):
            self.connection.close()   