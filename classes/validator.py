import re


class Validator:
    def __init__(self):
        self.password_min_length = 6
        self.username_max_length = 20
        self.regex_username = "^[A-Za-z0-9]*$"
        self.regex_email = "^[\w\.\+\-]+\@[\w]+\.[a-z]{2,3}$"

    def check_registration(self, form):
        errors_username = self._errors_username(form['username'])
        errors_email = self._errors_email(form['email'])
        errors_passwords = self._errors_passwords(form['password1'], form['password2'])

        errors = []
        errors.extend(errors_username)
        errors.extend(errors_email)
        errors.extend(errors_passwords)

        return errors

    def check_login(self, form):
        errors_email = self._errors_email(form['email'])
        errors = []
        errors.extend(errors_email)
        return errors

    def _errors_email(self, email):
        if bool(re.search(self.regex_email, email)):
            return []
        else:
            return ['Введите корректный email']

    def _errors_username(self, username):
        errors = []

        if len(username) == 0:
            errors.append('Введите имя')

        if len(username) > self.username_max_length:
            errors.append('Имя слишком длинное')

        if not re.match(self.regex_username, username):
            errors.append('Имя не может содержать спецсимволы и кириллицу')

        return errors


    def _errors_passwords(self, password1, password2):
        errors = []

        if len(password1) != len(password2):
            errors.append('Пароли не совпадают')

        if len(password1) == 0 and len(password2) == 0:
            errors.append('Пароли не введены')
        elif len(password1) < self.password_min_length or len(password2) < self.password_min_length:
            errors.append('Пароль слишком короткий')

        return errors