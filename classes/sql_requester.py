class SqlRequester:
    def __init__(self, db_cursor):
        self.db_cursor = db_cursor

    def clear_table(self, table_name):
        self.db_cursor.execute('DELETE FROM ' + table_name);

    def add_user(self, user):
        req = 'INSERT INTO users (email, username, password_hash, create_date) VALUES (%s, %s, %s, %s)'
        vals = (
            user['email'],
            user['username'],
            user['password_hash'],
            user['create_date']
        )
        self.db_cursor.execute(req, vals)

    def get_user_by_email(self, email):
        req = 'select * from users where email = %s'
        self.db_cursor.execute(req, (email,))
        return self.db_cursor.fetchall()